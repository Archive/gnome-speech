/*
 * GNOME Speech - Speech services for the GNOME desktop
 *
 * Copyright 2003 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * thetasynthesisdriver.h: Definition of the ThetaSynthesisDriver
 *                           object-- a GNOME Speech driver for Cepstral's
 *                           Theta TTS engine (implementation in
 *                           thetasynthesisdriver.c)
 *
 */

#ifndef __THETA_SYNTHESIS_DRIVER_H_
#define __THETA_SYNTHESIS_DRIVER_H_

#include <bonobo/bonobo-object.h>
#include <gnome-speech/gnome-speech.h>

#define THETA_SYNTHESIS_DRIVER_TYPE        (theta_synthesis_driver_get_type ())
#define THETA_SYNTHESIS_DRIVER(o)          (G_TYPE_CHECK_INSTANCE_CAST((o), THETA_SYNTHESIS_DRIVER_TYPE, ThetaSynthesisDriver))
#define THETA_SYNTHESIS_DRIVER_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), theta_SYNTHESIS_driver_TYPE, ThetaSynthesisDriverClass))
#define THETA_SYNTHESIS_DRIVER_GET_CLASS(obj)     (G_TYPE_INSTANCE_GET_CLASS((obj), THETA_SYNTHESIS_DRIVER_TYPE, ThetaSynthesisDriverClass))
#define IS_THETA_SYNTHESIS_DRIVER(o)       (G_TYPE_CHECK_INSTANCE_TYPE((o), THETA_SYNTHESIS_DRIVER_TYPE))
#define IS_THETA_SYNTHESIS_DRIVER_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE((k), THETA_SYNTHESIS_DRIVER_TYPE))

typedef struct {
	BonoboObject parent;

	gboolean initialized;
	gboolean speaking;
	GSList *utterances;
	 guint timeout_id;
	int status_pipe[2];
} ThetaSynthesisDriver;

typedef struct {
	BonoboObjectClass parent_class;

	POA_GNOME_Speech_SynthesisDriver__epv epv;
} ThetaSynthesisDriverClass;

#include "thetaspeaker.h"

GType
theta_synthesis_driver_get_type   (void);

ThetaSynthesisDriver *
theta_synthesis_driver_new (void);
gint
theta_synthesis_driver_say (ThetaSynthesisDriver *d,
			    ThetaSpeaker *s,
			    const gchar *text);
gboolean
theta_synthesis_driver_stop (ThetaSynthesisDriver *d);
gboolean
theta_synthesis_driver_is_speaking (ThetaSynthesisDriver *d);

#endif /* __THETA_SYNTHESIS_DRIVER_H_ */
